package com.brixzen.request;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.entity.mime.FormBodyPart;

/**
 * Created by brixzen on 28/03/2014.
 */
public class HttpRequest<T> {

    RequesMode requesMode;
    String baseurl;
    RequestParams requestParams;
    List<String> paramGet = new ArrayList<String>();
    List<String> valueGet = new ArrayList<String>();
    List<Param> paramList = new ArrayList<>();
    ProgressListener progressListener = null;
    RequestListener requestListener;
    static AsyncHttpClient asyncHttpClient;
    static SyncHttpClient syncHttpClient;
    public T t;
    boolean plaint = false;
    File file;
    boolean sync = true;

    private void initHttp(){
        if(sync){
            if(syncHttpClient==null){
                syncHttpClient = new SyncHttpClient();
                syncHttpClient.setTimeout(20000);
                syncHttpClient.setResponseTimeout(20000);
                syncHttpClient.setMaxRetriesAndTimeout(0,0);
                syncHttpClient.setMaxConnections(1);
            }
        }else{
            if(asyncHttpClient==null){
                asyncHttpClient = new AsyncHttpClient();
                asyncHttpClient.setTimeout(20000);
                asyncHttpClient.setResponseTimeout(20000);
                asyncHttpClient.setMaxRetriesAndTimeout(0,0);
                asyncHttpClient.setMaxConnections(1);
            }
        }
    }

    public HttpRequest(boolean sync,String baseurl,RequesMode requesMode, RequestListener requestListener, T t){
        this.sync = sync;
        this.requesMode = requesMode;
        this.requestListener = requestListener;
        this.baseurl = baseurl;
        this.t = t;
        initHttp();
        if(requesMode.equals(RequesMode.Post)){
            requestParams = new RequestParams();
        }
        plaint = false;
    }

    public HttpRequest(boolean sync,String baseurl,RequesMode requesMode, RequestListener requestListener){
        this.sync = sync;
        this.requesMode = requesMode;
        this.requestListener = requestListener;
        this.baseurl = baseurl;
        initHttp();
        if(requesMode.equals(RequesMode.Post)){
            requestParams = new RequestParams();
        }
        plaint = true;
    }

    public HttpRequest(boolean sync,String baseurl,RequesMode requesMode, RequestListener requestListener,File file){
        this.sync = sync;
        this.requesMode = requesMode;
        this.requestListener = requestListener;
        this.baseurl = baseurl;
        initHttp();
        this.file = file;
    }

    public void addProgressListener(ProgressListener progressListener){
        this.progressListener = progressListener;
    }

    public void addParameter(String post, String value){
        if(requesMode.equals(RequesMode.Post)){
            requestParams.add(post,value);
        }else if(requesMode.equals(RequesMode.Get)){
            paramGet.add(post);
            valueGet.add(value);
        }
    }

    public void addParameter(String post, File value){
        if(requesMode.equals(RequesMode.Post)){
            try {
                requestParams.put(post, value);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void addParameterListParam(ArrayList<Param> paramList){
        if(requesMode.equals(RequesMode.Post)){
            for (int n=0; n<paramList.size();n++){
                if(paramList.get(n).getValueFile()!=null){
                    addParameter(paramList.get(n).getName(),
                            paramList.get(n).getValueFile());
                }else{
                    addParameter(paramList.get(n).getName(),
                            paramList.get(n).getValueString());
                }
            }

        }
    }

    public void addParameterRequestBody(ArrayList<FormBodyPart> formBodyPartArrayList){
        if(requesMode.equals(RequesMode.Post)){
            for (int n=0; n<formBodyPartArrayList.size();n++){
//                if(formBodyPartArrayList.get(n).getBody().getFilename()!=null)
//                    requestParams.put(formBodyPartArrayList.get(n).getName().toString(), formBodyPartArrayList.get(n).getBody().getFilename());
//                else
//                    requestParams.put(formBodyPartArrayList.get(n).getName().toString(), );
            }

        }
    }

    public void execute(){
        if(requesMode.equals(RequesMode.Post)){
            if(sync){
                syncHttpClient.post(baseurl, requestParams, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                        if(throwable!=null){
                            requestListener.onError(throwable.getMessage());
                        }else{
                            requestListener.onError("");
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                        if (plaint) {
                            requestListener.onFinish(responseString);
                        } else {
                            Log.e("Plaint:",String.valueOf(responseString));
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            requestListener.onFinish(gson.fromJson(responseString, t.getClass()));
                        }
                    }

                    @Override
                    public void onProgress(long bytesWritten, long totalSize) {
                        super.onProgress(bytesWritten, totalSize);
                        if(progressListener!=null){
                            progressListener.transferred(bytesWritten,totalSize);
                        }
                    }
                });
            }else{
                asyncHttpClient.post(baseurl, requestParams, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                        if(throwable!=null){
                            requestListener.onError(throwable.getMessage());
                        }else{
                            requestListener.onError("");
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                        if (plaint) {
                            requestListener.onFinish(responseString);
                        } else {
                            Log.e("Plaint:",String.valueOf(responseString));
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            requestListener.onFinish(gson.fromJson(responseString, t.getClass()));
                        }
                    }

                    @Override
                    public void onProgress(long bytesWritten, long totalSize) {
                        super.onProgress(bytesWritten, totalSize);
                        if(progressListener!=null){
                            progressListener.transferred(bytesWritten,totalSize);
                        }
                    }
                });
            }
        }else if(requesMode.equals(RequesMode.Get)){
            for (int i = 0; i < paramGet.size(); i++) {
                if(i==0){
                    baseurl+="?"+paramGet.get(i)+"="+valueGet.get(i);
                }else{
                    baseurl+="&"+paramGet.get(i)+"="+valueGet.get(i);
                }
            }
            if(sync){
                syncHttpClient.get(baseurl, requestParams, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                        if(throwable!=null){
                            requestListener.onError(throwable.getMessage());
                        }else{
                            requestListener.onError("");
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                        if(plaint){
                            requestListener.onFinish(responseString);
                        }else{
                            Log.e("Plaint:",String.valueOf(responseString));
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            requestListener.onFinish(gson.fromJson(responseString, t.getClass()));
                        }
                    }

                    @Override
                    public void onProgress(long bytesWritten, long totalSize) {
                        super.onProgress(bytesWritten, totalSize);
                        if(progressListener!=null){
                            progressListener.transferred(bytesWritten,totalSize);
                        }
                    }
                });
            }else{
                asyncHttpClient.get(baseurl,requestParams,new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                        if(throwable!=null){
                            requestListener.onError(throwable.getMessage());
                        }else{
                            requestListener.onError("");
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                        if(plaint){
                            requestListener.onFinish(responseString);
                        }else{
                            Log.e("Plaint:",String.valueOf(responseString));
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            requestListener.onFinish(gson.fromJson(responseString, t.getClass()));
                        }
                    }

                    @Override
                    public void onProgress(long bytesWritten, long totalSize) {
                        super.onProgress(bytesWritten, totalSize);
                        if(progressListener!=null){
                            progressListener.transferred(bytesWritten,totalSize);
                        }
                    }
                });
            }

        }else if(requesMode.equals(RequesMode.Download)){
            syncHttpClient.get(baseurl,new FileAsyncHttpResponseHandler(file) {
                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, File file) {
                    if(throwable!=null){
                        requestListener.onError(throwable.getMessage());
                    }else{
                        requestListener.onError("");
                    }
                }

                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, File file) {
                    requestListener.onFinish(file);
                }

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    if(progressListener!=null){
                        progressListener.transferred(bytesWritten,totalSize);
                    }
                }
            });
        }
    }
}
