package com.brixzen.request;

/**
 * Created by brixzen on 28/03/2014.
 */
public interface RequestListener {
    public void onFinish(Object object);
    public void onError(String message);
}
