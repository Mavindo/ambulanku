package com.brixzen.request;

import java.io.File;

/**
 * Created by Ahmed Yusuf on 11/28/16.
 */

public class Param {
    String name;
    String valueString;
    File valueFile;

    public Param(String name, File valueFile) {
        this.name = name;
        this.valueFile = valueFile;
    }

    public Param(String name, String valueString) {
        this.name = name;
        this.valueString = valueString;
    }

    public Param(String name, Double valueDouble) {
        this.name = name;
        this.valueString = String.valueOf(valueDouble);
    }

    public Param(String name, int valueInt) {
        this.name = name;
        this.valueString = String.valueOf(valueInt);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

    public File getValueFile() {
        return valueFile;
    }

    public void setValueFile(File valueFile) {
        this.valueFile = valueFile;
    }
}
