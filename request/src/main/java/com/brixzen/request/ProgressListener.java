package com.brixzen.request;

/**
 * Created by brixzen on 29/03/2014.
 */
public interface ProgressListener {
    void transferred(long num, long max);
}
