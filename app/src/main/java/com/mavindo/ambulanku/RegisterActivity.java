package com.mavindo.ambulanku;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.brixzen.request.HttpRequest;
import com.brixzen.request.Param;
import com.brixzen.request.RequesMode;
import com.brixzen.request.RequestListener;
import com.google.gson.Gson;
import com.mavindo.ambulanku.entity.request.RequestLogin;
import com.mavindo.ambulanku.service.GpsTracker;
import com.mavindo.ambulanku.utils.UserPref;

import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity {

    Button buttonRegister;

    EditText txtInputLoginNama,txtInputLoginEmail,txtInputLoginPassword,txtInputLoginNoIdentitas,txtInputLoginHp;

    HttpRequest httpRequest;
    String responseString;
    static ProgressDialog progressDialog;

    boolean isConnected;
    String lattitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        buttonRegister = (Button) findViewById(R.id.btnRegister);

        txtInputLoginNama = (EditText) findViewById(R.id.txtInputLoginNama);
        txtInputLoginEmail = (EditText) findViewById(R.id.txtInputLoginEmail);
        txtInputLoginPassword = (EditText) findViewById(R.id.txtInputLoginPassword);
        txtInputLoginHp = (EditText) findViewById(R.id.txtInputLoginHp);
        txtInputLoginNoIdentitas = (EditText) findViewById(R.id.txtInputLoginNoIdentitas);

        // check if GPS enabled
        GpsTracker gpsTracker = new GpsTracker(this);

        if(gpsTracker.getIsGPSTrackingEnabled()){
            lattitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }


        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkConnection();
                if(isConnected) {
                    if (txtInputLoginNama.getText().toString().isEmpty()) {
                        Toast.makeText(RegisterActivity.this, "Harap Isi Nama Anda", Toast.LENGTH_SHORT);
                    } else if (txtInputLoginEmail.toString().isEmpty()) {
                        Toast.makeText(RegisterActivity.this, "Harap Isi Email Anda", Toast.LENGTH_SHORT);
                    }else if (txtInputLoginPassword.getText().toString().isEmpty()) {
                        Toast.makeText(RegisterActivity.this, "Harap Isi Password Anda", Toast.LENGTH_SHORT);
                    } else if (txtInputLoginNoIdentitas.toString().isEmpty()) {
                        Toast.makeText(RegisterActivity.this, "Harap Isi No Identitas Anda", Toast.LENGTH_SHORT);
                    }else if (txtInputLoginHp.toString().isEmpty()) {
                        Toast.makeText(RegisterActivity.this, "Harap Isi Nomor Hp Anda", Toast.LENGTH_SHORT);
                    } else {
                        new ProcessingRegisterNew().execute();
                    }
                }
            }
        });
    }

    private void checkConnection() {
        ConnectivityManager cm = (ConnectivityManager) getBaseContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnectedBool = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
        if (!isConnectedBool) {
            this.isConnected = false;
        } else {
            this.isConnected = true;
        }
    }

    private class ProcessingRegisterNew extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(RegisterActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Memproses data. Mohon tunggu.");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMax(100);
            progressDialog.show();
//
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        private String uploadFile() {


            String url_server = "http://ambulanku.xyz/api/register_user";


            ArrayList<Param> paramArrayList = new ArrayList<>();
            try {
//                paramArrayList.addAll(App.paramArrayList);
                paramArrayList.add(new Param("email", String.valueOf(txtInputLoginEmail.getText().toString())));
                paramArrayList.add(new Param("password", String.valueOf(txtInputLoginPassword.getText().toString())));
                paramArrayList.add(new Param("no_identitas", String.valueOf(txtInputLoginNoIdentitas.getText().toString())));
                paramArrayList.add(new Param("no_hp", String.valueOf(txtInputLoginHp.getText().toString())));
                paramArrayList.add(new Param("lat", String.valueOf(lattitude)));
                paramArrayList.add(new Param("long", String.valueOf(longitude)));
                paramArrayList.add(new Param("gcm_id", String.valueOf("qwQ2E1ASasacaswd")));

                for (int n=0;n<paramArrayList.size();n++){
                    if(BuildConfig.DEBUG) {
                        Log.e("Part " + paramArrayList.get(n).getName().toString(),
                                String.valueOf(paramArrayList.get(n).getValueString()));
                    }
                }

            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e("Register Exception","ProcessingRegister"+ e.getMessage().toString());
                }
            }
            responseString = "";

            httpRequest = new HttpRequest(true, url_server, RequesMode.Post, new RequestListener() {
                @Override
                public void onFinish(Object object) {
                    progressDialog.dismiss();
                    responseString = object.toString();
//                    Toast.makeText(LoginActivity.this,"Sukses "+responseString,Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(String message) {
                    progressDialog.dismiss();
                    if (BuildConfig.DEBUG) {
                        Log.e("ProcessLogin Result", message);
                    }
                }
            });
            httpRequest.addParameterListParam(paramArrayList);
            httpRequest.execute();
            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (BuildConfig.DEBUG) {
                Log.e("ProcessLogin Result", result);
            }

            try {
                Gson gson = new Gson();
                final RequestLogin jsonBiasaRequest = gson.fromJson(result, RequestLogin.class);
                if (jsonBiasaRequest.getResponse()) {
                    UserPref.setRegistered(getBaseContext(),true,txtInputLoginNama.getText().toString(),txtInputLoginEmail.getText().toString(),txtInputLoginPassword.getText().toString(),txtInputLoginNoIdentitas.getText().toString(),txtInputLoginHp.getText().toString());
                    UserPref.setUserId(getBaseContext(), jsonBiasaRequest.getIdUser());
                    Intent intent = new Intent(RegisterActivity.this,MenuActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(RegisterActivity.this,"Error 001: "+jsonBiasaRequest.getMessage(),Toast.LENGTH_SHORT);
                        }
                    });
                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e("ProcessLogin Exception", e.getMessage().toString());
                }
                Toast.makeText(RegisterActivity.this,"Error 002: Gagal Mengakses Server",Toast.LENGTH_SHORT);
            }
            super.onPostExecute(result);
        }

    }
}
