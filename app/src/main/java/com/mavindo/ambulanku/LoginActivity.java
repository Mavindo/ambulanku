package com.mavindo.ambulanku;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.brixzen.request.HttpRequest;
import com.brixzen.request.Param;
import com.brixzen.request.RequesMode;
import com.brixzen.request.RequestListener;
import com.google.gson.Gson;
import com.mavindo.ambulanku.entity.request.RequestLogin;
import com.mavindo.ambulanku.utils.UserPref;

import java.util.ArrayList;


public class LoginActivity extends AppCompatActivity {

    Button buttonSignUp, buttonSignIn;

    EditText inputEmail, inputPassword;

    HttpRequest httpRequest;
    String responseString;

    static ProgressDialog progressDialog;
    boolean isConnected = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_new);

        buttonSignUp = (Button) findViewById(R.id.button_signup);
        buttonSignIn = (Button) findViewById(R.id.button_signin);
        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);

        if(UserPref.getRegister(getBaseContext())){
            startActivity(new Intent(LoginActivity.this, MenuActivity.class));
            finish();
        }

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkConnection();
                if(isConnected) {
                    if (inputEmail.getText().toString().isEmpty()) {
                        Toast.makeText(LoginActivity.this, "Harap Isi Email Anda", Toast.LENGTH_SHORT);
                    } else if (inputPassword.getText().toString().isEmpty()) {
                        Toast.makeText(LoginActivity.this, "Harap Isi Password Anda", Toast.LENGTH_SHORT);
                    } else {
                        new ProcessingLoginNew().execute();
                    }
                }
//
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    private void checkConnection() {
        ConnectivityManager cm = (ConnectivityManager) getBaseContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnectedBool = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
        if (!isConnectedBool) {
            this.isConnected = false;
        } else {
            this.isConnected = true;
        }
    }

    private class ProcessingLoginNew extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Memproses data. Mohon tunggu.");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMax(100);
            progressDialog.show();
//
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        private String uploadFile() {


            String url_server = "http://ambulanku.xyz/api/login_user";


            ArrayList<Param> paramArrayList = new ArrayList<>();
            try {
//                paramArrayList.addAll(App.paramArrayList);
                paramArrayList.add(new Param("email", String.valueOf(inputEmail.getText().toString())));
                paramArrayList.add(new Param("password", String.valueOf(inputPassword.getText().toString())));
                for (int n=0;n<paramArrayList.size();n++){
                    if(BuildConfig.DEBUG) {
                        Log.e("Part " + paramArrayList.get(n).getName().toString(),
                                String.valueOf(paramArrayList.get(n).getValueString()));
                    }
                }

            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e("Register Exception","ProcessingRegister"+ e.getMessage().toString());
                }
            }
            responseString = "";

            httpRequest = new HttpRequest(true, url_server, RequesMode.Post, new RequestListener() {
                @Override
                public void onFinish(Object object) {
                    progressDialog.dismiss();
                    responseString = object.toString();
//                    Toast.makeText(LoginActivity.this,"Sukses "+responseString,Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(String message) {
                    progressDialog.dismiss();
                    if (BuildConfig.DEBUG) {
                        Log.e("ProcessLogin Result", message);
                    }
                }
            });
            httpRequest.addParameterListParam(paramArrayList);
            httpRequest.execute();
            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            if (BuildConfig.DEBUG) {
                Log.e("ProcessLogin Result", result);
            }
            progressDialog.dismiss();

            try {
                Gson gson = new Gson();
                final RequestLogin jsonBiasaRequest = gson.fromJson(result, RequestLogin.class);
                if (jsonBiasaRequest.getResponse()) {
                    UserPref.setLogin(getBaseContext(),true,inputEmail.getText().toString(),inputPassword.getText().toString(),jsonBiasaRequest.getIdUser());
                    UserPref.setUserId(getBaseContext(),jsonBiasaRequest.getIdUser());
                    Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                    startActivity(intent);
                    finish();
                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                           Toast.makeText(LoginActivity.this,"Error 001: "+jsonBiasaRequest.getMessage(),Toast.LENGTH_SHORT);
                        }
                    });
                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e("ProcessLogin Exception", e.getMessage().toString());
                }
                Toast.makeText(LoginActivity.this,"Error 002: Gagal Mengakses Server",Toast.LENGTH_SHORT);
            }
            super.onPostExecute(result);
        }

    }
}
