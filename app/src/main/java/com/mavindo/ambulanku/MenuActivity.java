package com.mavindo.ambulanku;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.brixzen.request.HttpRequest;
import com.brixzen.request.Param;
import com.brixzen.request.RequesMode;
import com.brixzen.request.RequestListener;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.mavindo.ambulanku.entity.KantorAdapter;
import com.mavindo.ambulanku.entity.content.ContentListAmbulance;
import com.mavindo.ambulanku.entity.content.KantorContent;
import com.mavindo.ambulanku.entity.request.RequestListAmbulance;
import com.mavindo.ambulanku.entity.request.RequestLogin;
import com.mavindo.ambulanku.service.GpsTracker;
import com.mavindo.ambulanku.utils.UserPref;

import java.util.ArrayList;
import java.util.List;

public class MenuActivity extends AppCompatActivity implements OnMapReadyCallback {
    ListView listViewKantor;
    GoogleMap googleMap;
    GoogleApiClient client;
    LocationManager locationManager;
    KantorAdapter adapter;
    List<ContentListAmbulance> content = new ArrayList<ContentListAmbulance>();

    Double lat, lon;
    LatLng myLoc;

    ArrayList<LatLng> MarkerPoints;
    LinearLayout layoutDetail;
    TextView txtDetailNama, txtDetailAlamat, txtDetailTelp;
    ImageView imgButtonCall;

    HttpRequest httpRequest;
    String responseString;

    static ProgressDialog progressDialog;
    boolean isConnected = false;

    String lattitude, longintude;

    Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kantor_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setContentInsetStartWithNavigation(0);
        setSupportActionBar(toolbar);

        listViewKantor = (ListView) findViewById(R.id.listViewKantor);
        MarkerPoints = new ArrayList<>();
        layoutDetail = (LinearLayout) findViewById(R.id.layoutDetailKantor);
        txtDetailNama = (TextView) findViewById(R.id.txtKantorDetailTitle);
        txtDetailAlamat = (TextView) findViewById(R.id.txtKantorDetailAlamat);
        txtDetailTelp = (TextView) findViewById(R.id.txtKantorDetailNoTelp);
        imgButtonCall = (ImageView) findViewById(R.id.btnCall);

        adapter = new KantorAdapter(MenuActivity.this, content, "Kantor Polisi");
        listViewKantor.setAdapter(adapter);

        // check if GPS enabled
        GpsTracker gpsTracker = new GpsTracker(this);

        if (gpsTracker.getIsGPSTrackingEnabled())
        {
            lattitude = String.valueOf(gpsTracker.getLatitude());
            longintude = String.valueOf(gpsTracker.getLongitude());

        }
        else
        {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }

        new ProcessingListAmbulance().execute();

        ((MapFragment) getFragmentManager().findFragmentById(
                R.id.map)).getMapAsync(this);
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        locationManager = (LocationManager) getBaseContext().getSystemService(MenuActivity.LOCATION_SERVICE);
        listViewKantor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MenuActivity.this, DetailActivity.class);
                intent.putExtra("nama", content.get(position).getNamaRs());
                intent.putExtra("alamat", content.get(position).getAlamat());
                intent.putExtra("email", content.get(position).getEmail());
                intent.putExtra("lat", content.get(position).getLat());
                intent.putExtra("long", content.get(position).getLong());
                intent.putExtra("idAmbulance", content.get(position).getIdAmbulance());
                startActivity(intent);
            }
        });
        listViewKantor.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount - firstVisibleItem < visibleItemCount + 2) {
                }
            }
        });

    }

    private void mAddMarker(String title, String latitude, String longitude, boolean focusOnThisMarker) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        MarkerOptions marker = new MarkerOptions().position(latLng)
                .title(title);
        // Getting URL to the Google Directions API
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_ambulance_40));
        googleMap.addMarker(marker);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        if (location != null)
        {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(10)                   // Sets the zoom
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        googleMap.setMyLocationEnabled(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        updateMarker();
    }

    public void updateMarker() {
        if (googleMap == null) {
            return;
        }
       runOnUiThread(new Runnable() {
            @Override
            public void run() {
                googleMap.clear();
                for (int i = 0; i < content.size(); i++) {
                    if (i == 0)
                        mAddMarker(content.get(i).getNamaRs(),
                                content.get(i).getLat(), content.get(i).getLong(), true);
                    else mAddMarker(content.get(i).getNamaRs(),
                            content.get(i).getLat(), content.get(i).getLong(), false);
                }
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

//                if (ActivityCompat.checkSelfPermission(MenuActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                    // TODO: Consider calling
//                    //    ActivityCompat#requestPermissions
//                    // here to request the missing permissions, and then overriding
//                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                    //                                          int[] grantResults)
//                    // to handle the case where the user grants the permission. See the documentation
//                    // for ActivityCompat#requestPermissions for more details.
//                    return;
//                }
//                googleMap.setMyLocationEnabled(true);
            }
        });

    }

    private class ProcessingListAmbulance extends AsyncTask<Void, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(MenuActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Memproses data. Mohon tunggu.");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMax(100);
            progressDialog.show();
//
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        private String uploadFile() {


            String url_server = "http://ambulanku.xyz/api/list_ambulance";


            ArrayList<Param> paramArrayList = new ArrayList<>();
            try {
//                paramArrayList.addAll(App.paramArrayList);
                paramArrayList.add(new Param("lat", String.valueOf(lattitude)));
                paramArrayList.add(new Param("long", String.valueOf(longintude)));
                paramArrayList.add(new Param("page", String.valueOf(1)));

                for (int n=0;n<paramArrayList.size();n++){
                    if(BuildConfig.DEBUG) {
                        Log.e("Part " + paramArrayList.get(n).getName().toString(),
                                String.valueOf(paramArrayList.get(n).getValueString()));
                    }
                }

            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e("Register Exception","ProcessingRegister"+ e.getMessage().toString());
                }
            }
            responseString = "";

            httpRequest = new HttpRequest(true, url_server, RequesMode.Post, new RequestListener() {
                @Override
                public void onFinish(Object object) {
                    progressDialog.dismiss();
                    responseString = object.toString();
//                    Toast.makeText(LoginActivity.this,"Sukses "+responseString,Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(String message) {
                    progressDialog.dismiss();
                    if (BuildConfig.DEBUG) {
//                        Log.e("ProcessLogin Result", message);
                    }
                }
            });
            httpRequest.addParameterListParam(paramArrayList);
            httpRequest.execute();
            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            if (BuildConfig.DEBUG) {
                Log.e("ProcessLogin Result", result);
            }
            progressDialog.dismiss();

            try {
                Gson gson = new Gson();
                final RequestListAmbulance jsonBiasaRequest = gson.fromJson(result, RequestListAmbulance.class);
                if (jsonBiasaRequest.getSuccess()) {
                    content.clear();
                    content.addAll(jsonBiasaRequest.getContent());
                    final KantorAdapter adapter = new KantorAdapter(MenuActivity.this, content, "Kantor Polisi");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listViewKantor.setAdapter(adapter);
                            //add all marker
                            for (int n = 0; n < jsonBiasaRequest.getContent().size(); n++) {
                                    mAddMarker(jsonBiasaRequest.getContent().get(n).getNamaRs(),
                                            jsonBiasaRequest.getContent().get(n).getLat(),
                                            jsonBiasaRequest.getContent().get(n).getLong(),
                                            true);
                            }
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MenuActivity.this,"Error 001: "+jsonBiasaRequest.getMessage(),Toast.LENGTH_SHORT);
                        }
                    });
                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e("ProcessLogin Exception", e.getMessage().toString());
                }
                Toast.makeText(MenuActivity.this,"Error 002: Gagal Mengakses Server",Toast.LENGTH_SHORT);
            }
            super.onPostExecute(result);
        }

    }

}
