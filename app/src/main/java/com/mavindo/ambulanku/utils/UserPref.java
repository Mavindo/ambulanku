package com.mavindo.ambulanku.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by AhmedYusuf on 7/16/16.
 */
public class UserPref {
    private static final String PREF = "registration";

    public static void setRegistered(Context context, boolean value,
                                     String nama,String email, String password, String noIdentitas, String noHp){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("nama", nama);
        editor.putString("email", email);
        editor.putString("password", password);
        editor.putString("noIdentitas", noIdentitas);
        editor.putString("noHp", noHp);
        editor.putBoolean("Registered", value);
        editor.commit();
    }

    public static void setLogin(Context context, boolean value,
                                      String email, String password, String iduser){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("email", email);
        editor.putString("password", password);
        editor.putString("noIdentitas", iduser);
        editor.putBoolean("Registered", value);
        editor.commit();
    }

    public static Boolean getRegister(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF, 0);
        return sharedPreferences.getBoolean("Registered",false);
    }

    public static void  setUserId(Context context, String userId){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("userId",userId);
        editor.commit();
    }

    public static String getUserId(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF, 0);
        return sharedPreferences.getString("userId","");
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        byte[] sha1hash = md.digest();
        return convertToHex(sha1hash);
    }


}
