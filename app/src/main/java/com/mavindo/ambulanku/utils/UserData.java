package com.mavindo.ambulanku.utils;


import java.io.File;
import java.util.ArrayList;

/**
 * Created by AhmedYusuf on 6/28/16.
 */
public class UserData {
    public static String KTP;
    public static String Nama;
    public static String Alamat;
    public static String NoHp;
    public static File fotoKTPFile;
    public static String fotoKTPFileName;
    public static Double latitude;
    public static Double longitude;
    public static Double accuracy;
    public static String gcmID;
    public static String gcmToken;
    public static String address;
    public static String kodepos;
    public static String chatAutoFill;
    public static String IpAddress = "192.168.43.233"; //used for udp

}
