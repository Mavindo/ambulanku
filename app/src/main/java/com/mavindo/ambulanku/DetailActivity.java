package com.mavindo.ambulanku;

import android.*;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brixzen.request.HttpRequest;
import com.brixzen.request.Param;
import com.brixzen.request.RequesMode;
import com.brixzen.request.RequestListener;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.mavindo.ambulanku.R;
import com.mavindo.ambulanku.entity.KantorAdapter;
import com.mavindo.ambulanku.entity.request.RequestListAmbulance;
import com.mavindo.ambulanku.entity.request.RequestLogin;
import com.mavindo.ambulanku.entity.request.RequestPesanAmbulance;
import com.mavindo.ambulanku.service.GpsTracker;
import com.mavindo.ambulanku.utils.GPSChecker;
import com.mavindo.ambulanku.utils.UserPref;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback{

        private GoogleMap googleMap;
        GPSChecker gps;
        Double lat, lon, myLat, myLong;
        ArrayList<LatLng> markerPoints;

        String gotResponse = "";

        String jarak, estimasi;
        LinearLayout layoutJarak, layoutEstimasi;
        TextView txtJarak, txtEstimasi;
        private ClipboardManager myClipboard;
        private ClipData myClip;

        Handler handler;

        AlertDialog alertDialogFailed;
        AlertDialog.Builder alertDialogBuilderFailed;

        Dialog dialogLoading;
        AlertDialog.Builder alertDialogBuilderLoading;
        TextView txtLoading;

        boolean gps_enabled;
        FloatingActionButton fabTelpon, fabChat, fabTelponSingle;
        FloatingActionMenu fabMenu;
        HttpRequest httpRequest;
        String responseString;

        static ProgressDialog progressDialog;
        boolean isConnected = false;
        String idUser, idAmbulance, pesan;

        Dialog dialogOrangDicari;
        AlertDialog alertDialog;
        AlertDialog.Builder alertDialogBuilder,alertDialogBuilderOrangDicari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kantor_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setContentInsetStartWithNavigation(0);
        setSupportActionBar(toolbar);
        TextView txtToolbar = (TextView) toolbar.findViewById(R.id.txtToolbarTitle);
        txtToolbar.setText("Detail Ambulan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (!BuildConfig.DEBUG) {
            alertDialogBuilder = new AlertDialog.Builder(DetailActivity.this);
            alertDialogBuilder.setTitle("Sedang Dalam Tahap Pengembangan")
                    .setMessage("Mohon maaf. Fitur ini sedang dalam tahap pengembangan.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();
                        }
                    });
            alertDialog = alertDialogBuilder.create();
        }

        gps_enabled = getIntent().getBooleanExtra("gps_enabled", false);
        //jika gagal mengambil lokasi dari gps, ambil dari preference
        // check if GPS enabled
        GpsTracker gpsTracker = new GpsTracker(this);

        if (gpsTracker.getIsGPSTrackingEnabled()) {
            myLat = Double.valueOf(gpsTracker.getLatitude());
            myLong = Double.valueOf(gpsTracker.getLongitude());

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }

        if (String.valueOf(gpsTracker.getLatitude()).equals("null")){
            if (BuildConfig.DEBUG) {
                Log.e(getLocalClassName(), "Error mengambil data GPS, ambil lokasi dari preference");
            }
        }else{

        }

        alertDialogBuilderFailed = new AlertDialog.Builder(DetailActivity.this);
        myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        gps = new GPSChecker(DetailActivity.this);
        markerPoints = new ArrayList<LatLng>();
        handler = new Handler();
        LinearLayout mapLayout = (LinearLayout) findViewById(R.id.mapLayout);
        ((MapFragment) getFragmentManager().findFragmentById(
                R.id.map)).getMapAsync(this);
        final TextView txtNama = (TextView) findViewById(R.id.txtKantorDetailTitle);
        final TextView txtAlamat = (TextView) findViewById(R.id.txtKantorDetailAlamat);
        final TextView txtNoTelp = (TextView) findViewById(R.id.txtKantorDetailNoTelp);
        fabChat = (FloatingActionButton) findViewById(R.id.fabChat);
        fabTelpon = (FloatingActionButton) findViewById(R.id.fabTelepon);
        fabMenu = (FloatingActionMenu) findViewById(R.id.fabMenu);
        fabTelponSingle = (FloatingActionButton) findViewById(R.id.fabTeleponSingle);
            fabTelponSingle.setVisibility(View.GONE);
            fabMenu.setVisibility(View.VISIBLE);
            fabChat.setVisibility(View.VISIBLE);
            fabChat.setLabelText("Pesan Ambulan");
            fabChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialogBuilderOrangDicari = new AlertDialog.Builder(DetailActivity.this);
                    dialogOrangDicari = alertDialogBuilderOrangDicari.create();
                    dialogOrangDicari = new Dialog(DetailActivity.this,
                            android.R.style.Theme_Translucent);
                    dialogOrangDicari.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogOrangDicari.setCancelable(true);
                    dialogOrangDicari.setContentView(R.layout.dialog_pesan_ambulance);
                    dialogOrangDicari.setCanceledOnTouchOutside(true);
                    dialogOrangDicari.show();
                    Button btnPesan = (Button) dialogOrangDicari.findViewById(R.id.button_pesan);
                    final EditText inputPesan = (EditText) dialogOrangDicari.findViewById(R.id.input_keterangan);
                    btnPesan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            pesan = inputPesan.getText().toString();
                            idAmbulance = getIntent().getStringExtra("idAmbulance");
                            idUser = UserPref.getUserId(getBaseContext());
                            new ProcessingPesanAmbulance().execute();
                        }
                    });

                }
            });
            fabTelpon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + String.valueOf(txtNoTelp.getText().toString())));
                    if (ActivityCompat.checkSelfPermission(getBaseContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.

                        return;
                    }
                    startActivity(callIntent);
                }
            });
        txtNama.setText(getIntent().getStringExtra("nama"));
        txtAlamat.setText(getIntent().getStringExtra("alamat"));
        txtNoTelp.setText(getIntent().getStringExtra("notelp"));
        txtNama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myClip = ClipData.newPlainText("text", txtNama.getText().toString());
                myClipboard.setPrimaryClip(myClip);
                Toast.makeText(getApplicationContext(), "Text Copied to clipboard",
                        Toast.LENGTH_SHORT).show();
            }
        });
        txtAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myClip = ClipData.newPlainText("text", txtAlamat.getText().toString());
                myClipboard.setPrimaryClip(myClip);
                Toast.makeText(getApplicationContext(), "Text Copied to clipboard",
                        Toast.LENGTH_SHORT).show();
            }
        });
        txtNoTelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + String.valueOf(txtNoTelp.getText().toString())));
                if (ActivityCompat.checkSelfPermission(getBaseContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);
            }
        });
        layoutEstimasi = (LinearLayout) findViewById(R.id.layoutDetailKantorEstimasi);
        layoutJarak = (LinearLayout) findViewById(R.id.layoutDetailKantorJarak);
        txtJarak = (TextView) findViewById(R.id.txtKantorDetailDistance);
        txtEstimasi = (TextView) findViewById(R.id.txtKantorDetailEstimasi);
        if (gps_enabled) {
            LatLng latLng = new LatLng(Double.parseDouble(getIntent().getStringExtra("lat")),
                    Double.parseDouble(getIntent().getStringExtra("long")));
            LatLng myLoc = new LatLng(myLat, myLong);
        }
    }

        private void updateMap() {
            if (googleMap == null) {
                return;
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    googleMap.clear();
                    mAddMarkerSelected(getIntent().getStringExtra("nama"), getIntent().getStringExtra("alamat"),
                            getIntent().getStringExtra("lat"), getIntent().getStringExtra("long"));
                    mAddMarkerSelected("Lokasi Anda", "",
                            "" + myLat, "" + myLong);
                    // set map type
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                    LatLng latLng = new LatLng(Double.parseDouble(getIntent().getStringExtra("lat")), Double.parseDouble(getIntent().getStringExtra("long")));
                    markerPoints.add(latLng);
                    if(gps_enabled){
                        LatLng myLoc = new LatLng(myLat, myLong);
                        markerPoints.add(myLoc);
                    }

//                LatLngBounds.Builder builder = new LatLngBounds.Builder();
//                for (Marker marker : markerPoints) {
//                    builder.include(marker.getPosition());
//                }
//                LatLngBounds bounds = builder.build();
//                int padding = 0; // offset from edges of the map in pixels
//                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

                    /** add Google Map Direction **/
                    //move map camera
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));

                    googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {

                        }
                    });
                }
            });
        }

        private void mAddMarkerSelected(String title, String address, String latitude, String longitude) {
            if (!latitude.isEmpty()) {
                lat = Double.parseDouble(latitude);
                lon = Double.parseDouble(longitude);
            }else {
                lat = -6.1234123;
                lon = 112.12341;
            }

            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            if (!title.equals("Lokasi Anda")) {
                MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lon))
                        .title(title);
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_ambulance_40));
                googleMap.addMarker(marker);
            } else {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);
            }
            if(BuildConfig.DEBUG)
                if(BuildConfig.DEBUG)
                    Log.e("add marker",title);
        }

        private String getUrl(LatLng origin, LatLng dest) {
            // Origin of route
            String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
            // Destination of route
            String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
            // Sensor enabled
            String sensor = "sensor=false";
            // Building the parameters to the web service
            String parameters = str_origin + "&" + str_dest + "&" + sensor;
            // Output format
            String output = "json";
            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
            return url;
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            this.googleMap = googleMap;
            updateMap();
        }

        @Override
        public void onPointerCaptureChanged(boolean hasCapture) {

        }


    private class ProcessingPesanAmbulance extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(DetailActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Memproses data. Mohon tunggu.");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMax(100);
            progressDialog.show();
//
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        private String uploadFile() {


            String url_server = "http://ambulanku.xyz/api/pesan";


            ArrayList<Param> paramArrayList = new ArrayList<>();
            try {
//                paramArrayList.addAll(App.paramArrayList);
                paramArrayList.add(new Param("id_user", String.valueOf(idUser)));
                paramArrayList.add(new Param("id_ambulance", String.valueOf(idAmbulance)));
                paramArrayList.add(new Param("pesan", String.valueOf(pesan)));
                paramArrayList.add(new Param("lat", String.valueOf(myLat)));
                paramArrayList.add(new Param("long", String.valueOf(myLong)));

                for (int n=0;n<paramArrayList.size();n++){
                    if(BuildConfig.DEBUG) {
                        Log.e("Part " + paramArrayList.get(n).getName().toString(),
                                String.valueOf(paramArrayList.get(n).getValueString()));
                    }
                }

            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e("Register Exception","ProcessingRegister"+ e.getMessage().toString());
                }
            }
            responseString = "";

            httpRequest = new HttpRequest(true, url_server, RequesMode.Post, new RequestListener() {
                @Override
                public void onFinish(Object object) {
                    progressDialog.dismiss();
                    responseString = object.toString();
//                    Toast.makeText(LoginActivity.this,"Sukses "+responseString,Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(String message) {
                    progressDialog.dismiss();
                    if (BuildConfig.DEBUG) {
                        Log.e("ProcessLogin Result", message);
                    }
                }
            });
            httpRequest.addParameterListParam(paramArrayList);
            httpRequest.execute();
            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            if (BuildConfig.DEBUG) {
                Log.e("ProcessLogin Result", result);
            }
            progressDialog.dismiss();

            try {
                Gson gson = new Gson();
                final RequestPesanAmbulance jsonBiasaRequest = gson.fromJson(result, RequestPesanAmbulance.class);
                if (jsonBiasaRequest.getResponse()) {
                    dialogOrangDicari.dismiss();
                    startActivity(new Intent(DetailActivity.this,WaitActivity.class));
                    finish();
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(DetailActivity.this,"Error 001: "+jsonBiasaRequest.getMessage(),Toast.LENGTH_SHORT);
                        }
                    });
                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e("ProcessLogin Exception", e.getMessage().toString());
                }
                Toast.makeText(DetailActivity.this,"Error 002: Gagal Mengakses Server",Toast.LENGTH_SHORT);
            }
            super.onPostExecute(result);
        }

    }


}
