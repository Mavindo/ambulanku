package com.mavindo.ambulanku.entity;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.mavindo.ambulanku.entity.content.ContentListAmbulance;
import com.mavindo.ambulanku.R;

import java.util.List;

/**
 * Created by AhmedYusuf on 6/28/16.
 */
public class KantorAdapter extends BaseAdapter {
    private Activity activity;
    private List<ContentListAmbulance> data;
    private static LayoutInflater inflater = null;
    Context context;
    private String jenisKantor;

    public KantorAdapter(Activity a, List<ContentListAmbulance> d, String jenisKantor){
        activity = a;
        data = d;
        this.jenisKantor = jenisKantor;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        context = a.getBaseContext();

    }

    public int getCount(){
        return data.size();
    }

    public Object getItem(int position){
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View view = convertView;
        ContentListAmbulance content = new ContentListAmbulance();

        if(convertView == null) {
                view = inflater.inflate(R.layout.row_kantor_polisi, null);
        }
//        RoundedImageView thumbsnail = (RoundedImageView) view.findViewById(R.id.thumbImage);
//        RelativeLayout titleLayout = (RelativeLayout)view.findViewById(R.id.titleLayout);
        TextView title = (TextView)view.findViewById(R.id.txtRowKantorTitle);
        TextView alamat = (TextView) view.findViewById(R.id.txtRowKantorAddress);

        content = data.get(position);
        String titles = data.get(position).getNamaRs();
        if(titles.length()>15){
            titles = titles.substring(0, 15);
        }
        title.setText(data.get(position).getNamaRs());
        alamat.setText(data.get(position).getAlamat());
        String latitude = data.get(position).getLat();
        String longitude = data.get(position).getLong();

        return view;
    }
}
