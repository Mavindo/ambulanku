
package com.mavindo.ambulanku.entity.request;

import java.util.List;

import com.mavindo.ambulanku.entity.content.ContentListAmbulance;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestListAmbulance {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("content")
    @Expose
    private List<ContentListAmbulance> content = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ContentListAmbulance> getContent() {
        return content;
    }

    public void setContent(List<ContentListAmbulance> content) {
        this.content = content;
    }

}
