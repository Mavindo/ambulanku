package com.mavindo.ambulanku.entity.content;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by AhmedYusuf on 5/31/16.
 */

@Generated("org.jsonschema2pojo")
public class KantorContent {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("long")
    @Expose
    private String _long;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("no_telp")
    @Expose
    private String noTelp;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("foto")
    @Expose
    private String foto;
    @SerializedName("kantor_type_id")
    @Expose
    private String kantorTypeId;
    @Expose
    @SerializedName("chat")
    String chat;

    public KantorContent() {
    }

    public KantorContent(String nama, String alamat, String lat, String _long, String chat) {
        this.nama = nama;
        this.alamat = alamat;
        this.lat = lat;
        this._long = _long;
        this.chat = chat;
    }

    public KantorContent(String id, String lat, String _long, String nama, String noTelp, String alamat, String foto, String kantorTypeId, String chat) {
        this.id = id;
        this.lat = lat;
        this._long = _long;
        this.nama = nama;
        this.noTelp = noTelp;
        this.alamat = alamat;
        this.foto = foto;
        this.kantorTypeId = kantorTypeId;
        this.chat = chat;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The lat
     */
    public String getLat() {
        return lat;
    }

    /**
     *
     * @param lat
     * The lat
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     *
     * @return
     * The _long
     */
    public String getLong() {
        return _long;
    }

    /**
     *
     * @param _long
     * The long
     */
    public void setLong(String _long) {
        this._long = _long;
    }

    /**
     *
     * @return
     * The nama
     */
    public String getNama() {
        return nama;
    }

    /**
     *
     * @param nama
     * The nama
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     *
     * @return
     * The noTelp
     */
    public String getNoTelp() {
        return noTelp;
    }

    /**
     *
     * @param noTelp
     * The no_telp
     */
    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    /**
     *
     * @return
     * The alamat
     */
    public String getAlamat() {
        return alamat;
    }

    /**
     *
     * @param alamat
     * The alamat
     */
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    /**
     *
     * @return
     * The foto
     */
    public String getFoto() {
        return foto;
    }

    /**
     *
     * @param foto
     * The foto
     */
    public void setFoto(String foto) {
        this.foto = foto;
    }

    /**
     *
     * @return
     * The kantorTypeId
     */
    public String getKantorTypeId() {
        return kantorTypeId;
    }

    /**
     *
     * @param kantorTypeId
     * The kantor_type_id
     */
    public void setKantorTypeId(String kantorTypeId) {
        this.kantorTypeId = kantorTypeId;
    }

    public String getChat() {
        return chat;
    }

    public void setChat(String chat) {
        this.chat = chat;
    }
}
