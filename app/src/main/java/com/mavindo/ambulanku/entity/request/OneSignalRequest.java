package com.mavindo.ambulanku.entity.request;

/**
 * Created by AhmedYusuf on 8/5/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class OneSignalRequest {

    @SerializedName("newsid")
    @Expose
    private String newsid;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("kota")
    @Expose
    private String kota;

    /**
     *
     * @return
     * The newsid
     */
    public String getNewsid() {
        return newsid;
    }

    /**
     *
     * @param newsid
     * The newsid
     */
    public void setNewsid(String newsid) {
        this.newsid = newsid;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }
}